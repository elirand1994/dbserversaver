import express from "express";
import cors from "cors";
import morgan from "morgan";
import log from "@ajar/marker";
import {env} from "../utils/env.js";
import backupRouter from "../routes/backup.route.js";
class App {
    private app : express.Application;
    constructor() {
        this.app = express();
        this.initAppDefaults();
    }

    private initAppDefaults = () =>{
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(morgan("dev"));
        this.app.use(cors());
        this.app.use("/backup",backupRouter);
    }

    public start = async (): Promise<void> => {
        await this.app.listen(env.PORT, env.HOST);
        log.magenta(
            "server is live on",
            ` ✨ ⚡  http://${env.HOST}:${env.PORT} ✨ ⚡`
        );
    };
}

const server = new App();
export default server;