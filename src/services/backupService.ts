import { exec } from "child_process";
import { Request, Response } from "express";
import fs from "fs";
class BackupService {
    localBackup = async (req : Request,res : Response) =>{
        const fileName = req.headers.filename;
        console.log(fileName)
        const stream = fs.createWriteStream(`backups/${fileName} - ${Date.now()}.sql`, 'utf-8');
        req.pipe(stream);
    }
}

const backupService = new BackupService();
export default backupService;