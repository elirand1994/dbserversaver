import { Request, Response } from "express";
import backupService from "../services/backupService.js";
class BackupController {
    backupHandler = async (req : Request, res: Response) =>{
        return await backupService.localBackup(req,res);
    }
}

const backupController = new BackupController();
export default backupController;