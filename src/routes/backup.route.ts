import backupController from "../controller/backup.controller.js";
import express from "express";
const router = express.Router();
router.use(express.json());


router.post("",backupController.backupHandler);

export default router;